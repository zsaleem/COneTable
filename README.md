## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - HTML.
 - Sass
 - ES6
 - ES6 Promises
 - Git
 - Gitlab
 - NPM
 - Vim
 - Mac OS X.
 - Google Chrome Incognito
 - Webpack
 - Babel

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. Run npm install command to download and install all Webpack dependencies.
    3. Run `npm run dev` to run the server on port `2770`. This command will run the project in development mode.
    4. To make it ready for deploy run `npm run build`. This command will minify all the resources in `/dist` folder.

## Description
Above steps, in getting started section, will install all dependencies required for this project to run and make the project ready for
production by minifying all resources. It will place the production ready project in `dist` folder at `root` level.

In this task, I followed MV* pattern. I created 2 folders inside `src/scripts/` i.e. `models` and `views`. Inside `models` I wrote 1 model i.e. `SolutionModel` which is responsible for getting data from provided json files inside `src/dataset/` folder. In addition, `SolutionModel` also sorts the data according to columns i.e. when user clicks on any column header then the sorting logic happens inside `model` and when the data is sorted the table is rerendered in `view`.

In `views` folder, I wrote 1 view i.e. `SolutionView`. I feed data to this view from its respective `model`(mentioned in previous paragraph). This view requires 2 parameters at the time of initialization(which happens in `src/index.js` file). First parameter is the model which is `SolutionModel` in this case and second is an object consists of the DOM element where the table will be rendered and the url from where the data will be loaded. Then in the render function inside view, it calls model's get function to load data, then it generates table with the data received from the `model`.

I wrote all CSS(Sass) in `src/styles/sass` folder. I created 1 file i.e. `solution.scss` which consists of all styles for this table.

I tested this task on Google Chrome Incognito and MacBook Air with a resolution of 1440x900.

## Notes
Since the task description mentions that the `javascript` file should be called as `solution.js` however, I followed proper MV* design pattern for maintainability and scalability purposes therefore, I prefixed both `Model` and `View` with `Solution` word.

Moreover, it is also mentioned in task description that the solution should be in pure javascript. I used ES6 standards with babel for legacy and all browser support. Since ES6 is also pure javascript therefore, I say my solution is entirly based on Pure JavaScript

Finally, I am sending you the link(please copy and paste the link) of the repository where I pushed my solution. I believe this is more professional way of submitting techincal tasks solution therefore, I chose this approach.

